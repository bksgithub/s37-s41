//import mongoose
const mongoose = require("mongoose")

//set up schema
const user_schema = new mongoose.Schema({
	firstName: {
		type: String,
		required: [true, "First name is required"]
	},
	lastName: {
		type: String,
		require: [true, "Last Name is required"]
	},
	email: {
		type: String,
		required: [true, "Email is required."]
	},
	password: {
		type: String, 
		required: [true, "Password is required."]
	},
	isAdmin: {
		type: Boolean,
		default: false
	},
	mobileNo: {
		type: String,
		required: [true, "Mobile number is required."]
	},
	enrollments: [
		{
			courseID: {
				type: String,
				required: [true, "Course ID is required."]
			},
			enrolledOn: {
				type: Date,
				default: new Date()
			},
			status: {
				type: String, 
				default: "Enrolled"
			}
		}
	]
})  

module.exports = mongoose.model("Users", user_schema)
