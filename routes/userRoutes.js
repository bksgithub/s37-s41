//imports
const express = require("express")
const router = express.Router()
const UserController = require("../controllers/userController")
const auth = require("../auth")

//check if the email exists
router.post("/check-email", (request, response) => {
	UserController.checkIfEmailExists(request.body).then((result) => {
		response.send(result)
	})
})

// register new user
router.post("/register", (request, response) => {
	UserController.register(request.body).then((result) => {
		response.send(result)
	})
})

// login user
router.post("/login", (request, response) => {
	UserController.login(request.body).then((result) => {
		response.send(result)
	})
})

// getting a single user details
router.get("/:id/details", auth.verify, (request, response) => {
	UserController.getUserDetails(request.params.id).then((result) => {
		response.send(result)
	})
})

// enroll a user
router.post("/enroll", auth.verify, (request, response) => {
	let data = {
		userId: request.body.userId,
		courseID: request.body.courseID
	}

	UserController.enroll(data).then((result) => {
		response.send(result)
	})
})

module.exports = router