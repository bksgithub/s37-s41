const express = require("express")
const router = express.Router()
const CourseController = require("../controllers/courseController")
const auth = require("../auth")

//create single course
router.post("/create", auth.verify, (request, response) => {

	const data = {
		course: request.body,
		isAdmin: auth.decode(request.headers.authorization).isAdmin
	}

	CourseController.addCourse(data).then((result) => {
		response.send(result)
	})
})

//get all courses
router.get("/", (request, response) => {
	CourseController.getAllCourses().then((result) => {
		response.send(result)
	})
})

//get all active courses
router.get("/active", (request, response) => {
	CourseController.getAllActive().then((result) => {
		response.send(result)
	})
})

//get single course
router.get("/:courseId", (request, response) => {
	CourseController.getCourse(request.params.courseId).then((result) => {
		response.send(result)
	})
})

//update single course
router.patch("/:courseId/update", auth.verify, (request, response) => {
	CourseController.updateCourse(request.params.courseId, request.body).then((result) => {
		response.send(result)
	})
})


//archive single course
router.patch("/:courseId/archive", auth.verify, (request, response) => {
	CourseController.archive(request.params.courseId, request.body).then((result) => {
		response.send(result)
	})
})
module.exports = router