//imports
const express = require("express")
const dotenv = require("dotenv")
const mongoose = require("mongoose")
const cors = require("cors")
const userRoutes = require("./routes/userRoutes")
const courseRoutes = require("./routes/courseRoutes")
//imports end

//initialize dotenv
dotenv.config()

const app = express()
const port = 8001 //setup port

//mongodb connection
mongoose.connect(`mongodb+srv://bks0381:${process.env.MONGODB_PASSWORD}@cluster0.0atlgec.mongodb.net/booking-system-api?retryWrites=true&w=majority`, {
	useNewUrlParser: true,
	useUnifiedTopology: true
})
//mongodb connection end

let db = mongoose.connection

db.once("open", () => console.log(`connected to mongodb`))

//initialize express
app.use(express.json())
app.use(express.urlencoded({extended: true}))
//initialize cors
app.use(cors())

//Routes
app.use("/users", userRoutes)
app.use("/courses", courseRoutes)
//Routes end

app.listen(port, () => console.log(`API is now running on localhost: ${port}.`))