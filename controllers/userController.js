//import model
const User = require("../models/users")
const Course = require("../models/course")
//bcrypt
const bcrypt = require("bcrypt")

//web token
const auth = require("../auth")

module.exports.checkIfEmailExists = (data) => {
	return User.find({
		email: data.email
	}).then((result) => {
		if (result.length > 0) {
			return true
		}

		return false
	})
}

module.exports.register = (data) => {

	//encrypting password using bcrypt
	let encrypted_password = bcrypt.hashSync(data.password, 10)

	//new instance of User
	let new_user = new User({
		firstName: data.firstName,
		lastName: data.lastName,
		email: data.email,
		mobileNo: data.mobileNo,
		password: encrypted_password
	})

	return new_user.save().then((created_user, error) => {
		if (error) {
			return false
		}

		return {
			message: "User successfully registered."
		}
	})
}

module.exports.login = (data) => {
	return User.findOne({email: data.email}).then((result) => {
		if (result == null) {
			return{
				message: "User does not exist."
			}
		}

		const is_password_correct = bcrypt.compareSync(data.password, result.password)

		if (is_password_correct) {
			return {
				access: auth.createAccessToken(result)
			}
		}

		return {
			message: "Password is incorrect."
		}
	})
}

module.exports.getUserDetails = (user_id) =>  {
	return User.findById(user_id, {password: 0}).then((error, result) => {
		if (error) {
			console.log(error)
			return error
		}

		return result
	
	})
}

module.exports.enroll = async (data) =>
//// check if user is done adding the course to its enrollments array
 { 

 	let is_user_updated = await User.findById(data.userId).then((user) => {
		user.enrollments.push({
		courseID: data.courseID
	})

	return user.save().then((result, error) => {
		if (error) {
			return false
		}

		return true
	})
})

//check if course id done adding the user to its enrollees array
let is_course_updated = await Course.findById(data.courseID).then((course) => {
	course.enrollees.push({
		userId: data.userId
	})

	return course.save().then((result, error) => {
		if (error) {
			return false
		}

		return true
	})
})

//check if both the user and course have been updated successfully and return a success message if so
if (is_user_updated && is_course_updated) {
	return {
		message: "User enrollment is successful"
	}
}

// if the enrollment failed, return something went wrong
return {
	message: "Something went wrong."
}
}
	
